﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using RazorDrop.Data;
using RazorDrop.ViewModels;
using System.IO;

namespace RazorDrop.Pages.Customers
{
    public class CreateModel : PageModel
    {
        private readonly RazorDropContext _context;

        [BindProperty(SupportsGet = true)]
        public CustomerEditViewModel CustomerEditViewModel { get; set; }

        public CreateModel(RazorDropContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            var repo = new CustomersRepository(_context);
            CustomerEditViewModel = repo.CreateCustomer();
            return Page();
        }

        public IActionResult OnPost()
        {
            try
            {
                if(ModelState.IsValid)
                {
                    var repo = new CustomersRepository(_context);
                    bool saved = repo.SaveCustomer(CustomerEditViewModel);
                    if (saved)
                    {
                        return RedirectToAction("Index");
                    }
                }

                throw new ApplicationException("Invalid Model");
            }
            catch(ApplicationException ex)
            {
                Debug.Write(ex.Message);
                throw ex;
            }
            
        }

        public IActionResult OnPostRegions()
        {
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyToAsync(stream);
            stream.Position = 0;
            using StreamReader reader = new StreamReader(stream);
            string requestBody = reader.ReadToEnd();
            if(requestBody.Length > 0)
            {
                var repo = new RegionsRepository(_context);

                IEnumerable<SelectListItem> regions = repo.GetRegions(requestBody);
                return new JsonResult(regions);
            }
            return null;

        }
    }
}