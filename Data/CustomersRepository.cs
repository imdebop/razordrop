﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using RazorDrop.Models;
using RazorDrop.ViewModels;

namespace RazorDrop.Data
{
    public class CustomersRepository
    {
        private readonly RazorDropContext _context;
        public CustomersRepository(RazorDropContext context)
        {
            _context = context;
        }

        public List<CustomerDisplayViewModel> GetCustomers()
        {
            List<Customer> customers = new List<Customer>();
            customers = _context.Customers.AsNoTracking()
                .Include(x => x.Country)
                .Include(x => x.Region)
                .ToList();

            if (customers != null)
            {
                List<CustomerDisplayViewModel> customerDisplay = new List<CustomerDisplayViewModel>();
                foreach (var x in customers)
                {
                    var _customerDisplay = new CustomerDisplayViewModel()
                    {
                        CustomerId = x.CustomerId,
                        CustomerName = x.CustomerName,
                        CountryName = x.Country.CountryNameEnglish,
                        RegionName = x.Region.RegionNameEnglish
                    };
                    customerDisplay.Add(_customerDisplay);
                }
                return customerDisplay;

            }
            return null;
        }

        public CustomerEditViewModel CreateCustomer()
        {
            var cRepo = new CountriesRepository(_context);
            var rRepo = new RegionsRepository(_context);
            var customer = new CustomerEditViewModel()
            {
                CustomerId = Guid.NewGuid().ToString(),
                Countries = cRepo.GetCountries(),
                Regions = rRepo.GetRegions()
            };
            return customer;
        }

        public bool SaveCustomer(CustomerEditViewModel customerEdit)
        {
            if(customerEdit != null)
            {
                if(Guid.TryParse(customerEdit.CustomerId, out Guid newGuid))
                {
                    var customer = new Customer()
                    {
                        CustomerId = newGuid,
                        CustomerName = customerEdit.CustomerName,
                        CountryId = customerEdit.SelectedCountryId,
                        RegionId = customerEdit.SelectedRegionId
                    };
                    customer.Country = _context.Countries.Find(customerEdit.SelectedCountryId);
                    customer.Region = _context.Regions.Find(customerEdit.SelectedRegionId);

                    _context.Customers.Add(customer);
                    _context.SaveChanges();
                    return true;
                }
            }
            //return false if customerEdit == null or CustomerId is not a Guid
            return false;
        }
    }
}
